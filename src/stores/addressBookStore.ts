import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
    id: number
    name: string
    tel: string
    gender: string
  }

export const useAddressBookStore = defineStore('address_book', () => {
    
    let lastId = 1
    const address = ref<AddressBook>({
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    })
    const addressList = ref<AddressBook[]>([])
    const isAddNew = ref(false)
    function clear() {
      address.value = {
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
      }
      isAddNew.value = false
    }
    function save() {
      if (address.value.id > 0) {
        //edit
        const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
        addressList.value[editedIndex] = address.value
      } else {
        addressList.value.push({ ...address.value, id: lastId++ })
      } 
      clear()
    }
    const edit = (id: number) => {
      isAddNew.value = true
      const editedIndex = addressList.value.findIndex((item) => item.id === id)
      address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
    }
    const remove = (id: number) => {
      const removeIndex = addressList.value.findIndex((item) => item.id === id)
      addressList.value.splice(removeIndex, 1)
    }
  return {address, addressList, isAddNew, save, clear, edit, remove}
})
